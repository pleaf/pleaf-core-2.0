<?php
/**
 * Created by Widana.
 * User: sts
 * Date: 10/04/18
 * Time: 14:25
 */

namespace Sts\PleafCore;

use Illuminate\Database\Eloquent\Model;

class BaseEntity extends Model
{

    public $caseAttribute = "";

    public function getAttribute($key)
    {
        if (array_key_exists($key, $this->relations)) {
            return parent::getAttribute($key);
        } else {
            return parent::getAttribute(snake_case($key));
        }
    }

    public function setAttribute($key, $value)
    {
        return parent::setAttribute(snake_case($key), $value);
    }

    public function toArray($case = null)
    {
        $attributes = $this->attributesToArray($this->caseAttribute);
        return array_merge($attributes, $this->relationsToArray($this->caseAttribute));
    }

    public function attributesToArray($case = null)
    {
        $attributes = parent::attributesToArray();
        return $this->array_change_key_case($attributes, $case);
    }

    public function relationsToArray($case = null)
    {
        $attributes = parent::relationsToArray();
        return $this->array_change_key_case($attributes, $case);
    }

    private function array_change_key_case(&$array, $case = null)
    {

        foreach ($array as $key => $value) {
            switch (strtoupper($case)) {
                case 'UPPER':
                    $caseKey = strtoupper($key);
                    break;
                case 'LOWER':
                    $caseKey = strtolower($key);
                    break;
                case 'CAMEL':
                    $caseKey = camel_case($key);
                    break;
                case 'SNAKE':
                    $caseKey = snake_case($key);
                    break;
                case 'STUDLY':
                    $caseKey = studly_case($key);
                    break;
                case 'UCFIRST':
                    $caseKey = ucfirst($key);
                    break;
                default:
                    $caseKey = $key;
            }
            // Change key if needed
            if ($caseKey != $key) {
                unset($array[$key]);
                $array[$caseKey] = $value;
                $key = $caseKey;
            }
            // Handle nested arrays
            if (is_array($value)) {
                $this->array_change_key_case($array[$key], $case);
            }
        }
        return $array;
    }
}