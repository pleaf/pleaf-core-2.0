<?php
namespace Sts\PleafCore\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

class CreateBasicBfById extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'leaf:create-basic-bf-id {package} {model} {dir?} {author?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a Basic Business Function By Id {location business object} {location model} {author}';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $package = $this->argument("package");
        $model = $this->argument("model");
        $dir = $this->argument("dir");
        $author = $this->argument("author");

        $result = collect(config("leaf_command"))->where("name", $package)->first();

        if(is_null($result)){
            $this->error("Package $package not found");
            return;
        }

        $this->info("Process generate Basic BF");

        $outputDir = $result["dir"]["bo"];
        $exceptionDir = $result["constantException"];
        if(!is_null($dir)) {
            $outputDir = $outputDir."/".$dir;
        }

        $namespaceBo = $result["namespace"]["bo"];
        $namespace = $result["namespace"]["model"];

        $listOfFile = [
            "Find".$model."ById",
            "Is".$model."ExistsById",
            "Val".$model."ExistsById",
        ];

        $someNamespaceModel = $namespace."\\".$model;
        $someModel = new $someNamespaceModel;
        $primaryKey = $someModel::getPrimaryKey();

        $input = [
            "var" => lcfirst($model),
            "model" => $model,
            "namespace" => $namespaceBo,
            "namespaceModel" => $someNamespaceModel,
            "author" => $author,
            "condition" => "where('$primaryKey', $".'id'.")->first()",
            "paramKeyValue" => "\n\t\t$"."id"." = "."$".'dto["id"];',
            "paramException" => "$"."id"
        ];

        $findExcConstant = strtoupper($model)."_ID_NOT_FOUND";
        $findExcString = ucfirst($model)." ID {0} not found";
        $valExcConstant = strtoupper($model)."_DOES_NOT_EXISTS";
        $valExcString = ucfirst($model)." does not exists with ID {0}";

        $template = null;
        foreach ($listOfFile as $value) {

            if($value == "Find".$model."ById") {
                $template = "pleaf-core::templates/create-basic-bf-find";
            } else if($value == "Is".$model."ExistsById") {
                $template = "pleaf-core::templates/create-basic-bf-is";
            } else if($value == "Val".$model."ExistsById") {
                $template = "pleaf-core::templates/create-basic-bf-val";
            }

            $input["className"] = $value;
            $input["findExcConstant"] = $findExcConstant;
            $input["findExcString"] = $findExcString;
            $input["valExcConstant"] = $valExcConstant;
            $input["valExcString"] = $valExcString;

            $view = view($template, $input);

            $path = $outputDir."/".$value.".php";

            if(!is_null($template)) {
                $this->generateFile($path, $view->render());
            }

            $this->info("Generated File: ". $path);

        }

        $explodeDirException = explode("/", $exceptionDir);
        $fileNameExc = array_splice($explodeDirException, count($explodeDirException)-1, 1);
        $isDirException = str_replace("/".$fileNameExc[0], "", $exceptionDir);

        if(!is_dir($isDirException)) {
            mkdir($isDirException);
            if(!file_exists($exceptionDir)) {
                $file = fopen($exceptionDir, "w");
                $textLine = "<?php\n";
                fwrite($file, $textLine);
                fclose($file);
            }
        }

        $path = fopen($exceptionDir, "a");
        $pathRead = fopen($exceptionDir, "r");
        $textData = fread($pathRead,filesize($exceptionDir));
        $text1 = "\nIF(!defined".'("'.$findExcConstant.'"))'."\n"."\tdefine".'("'.$findExcConstant.'"'.",".'"'.$findExcString.'");';
        $text2 = "\nIF(!defined".'("'.$valExcConstant.'"))'."\n"."\tdefine".'("'.$valExcConstant.'"'.",".'"'.$valExcString.'");';

        if (!self::contains($textData, $findExcConstant)) {
            fwrite($path, $text1);
        }

        if (!self::contains($textData, $valExcConstant)) {
            fwrite($path, $text2);
        }

        fclose($pathRead);
        fclose($path);

        $this->info("Done generate Basic BF");
    }

    private function generateFile ($path, $content){
        $f = fopen($path, "w");
        fwrite($f,"<?php\n\n");
        fwrite($f, $content);
        fclose($f);
    }

    private function contains($haystack, $needle, $caseSensitive = false) {
        return $caseSensitive ?
            (strpos($haystack, $needle) === FALSE ? FALSE : TRUE):
            (stripos($haystack, $needle) === FALSE ? FALSE : TRUE);
    }
}
