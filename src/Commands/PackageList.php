<?php
/**
 * Created by PhpStorm.
 * @author: Widana <widananurazis@gmail.com>
 * Date: 06/04/16
 */

namespace Sts\PleafCore\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\DB;

class PackageList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'leaf:package-list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a Model';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("............................................. List Packages  .............................................");

        $packageList = config("leaf_command");

        $lineNo = 0;
        foreach ($packageList as $value) {
            $lineNo++;
            $this->info("$lineNo. ".$value["name"]);
        }

    }

}
