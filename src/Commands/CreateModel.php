<?php
/**
 * Created by PhpStorm.
 * @author: Widana <widananurazis@gmail.com>
 * Date: 06/04/16
 */

namespace Sts\PleafCore\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\DB;
use Artisan;

class CreateModel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'leaf:create-model {table} {package} {className} {attribute?} {author?} {--interactive=true}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a Model';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $tableName = $this->argument("table");
        $package = $this->argument("package");
        $author = $this->argument("author");
        $attribute = $this->argument("attribute");
        $className = $this->argument("className");

        $result = collect(config("leaf_command"))->where("name", $package)->first();

        if(is_null($result)){
            $this->error("Package $package not found");
            return;
        }

        $this->info("Process generate model for package $package");

        $outputDir = $result["dir"]["model"];
        $namespace = $result["namespace"]["model"];

        $findTable = collect(\DB::Select(
            "SELECT a.attname, format_type(a.atttypid, a.atttypmod) AS data_type
                        FROM   pg_index i
                        JOIN   pg_attribute a ON a.attrelid = i.indrelid
                                    AND a.attnum = ANY(i.indkey)
                        WHERE  i.indrelid = '".$tableName."'::regclass
                                    AND    i.indisprimary"
        ))->first();

        $replace = ucwords(str_replace("_"," ",$tableName));
        $explode = explode(" ", $replace);
//        $fileName = implode(array_slice($explode, 1));
        $fileName = $className;

        $view = view("pleaf-core::templates/create-model", [
            "name" => $fileName,
            "namespace" => $namespace,
            "primary" => $findTable->attname,
            "table" => $tableName,
            "author" => $author,
            "attribute" => !is_null($attribute)? strtoupper($attribute) : "SNAKE"
        ]);

        $path = $outputDir."/$fileName.php";

        $isGenerated = false;
        if(file_exists($path)) {
            $this->error("File already exists");
            if ($this->confirm("Overwrite? [y|N]")) {
                $isGenerated = true;
            }

        } else {
            $isGenerated = true;
        }

        if ($isGenerated) {
            $this->generateFile($path, $view->render());
            $this->info("Done generate Model for package $package");
            $this->info("File: $path");

            $interactive = $this->option("interactive");

            do {

                if($interactive) {

                    if($this->confirm("Are you generate Basic BF? [y/n]")) {

                        $outputDirBoDir = $result["dir"]["bo"];
                        $this->info("Default location: ". $outputDirBoDir);
                        if(!$this->confirm("What you want to use the default directory BusinessObjects [y/n]")) {
                            $locationBo = $this->ask("What Directory BusinessObjects?");

                            $this->info("Output BusinessObjects Directory: ". $outputDirBoDir."/".$locationBo);
                            self::actionBusinessObjectsGenerated($package, $fileName, $locationBo);

                        } else {
                            $this->info("Output BusinessObjects Directory: ". $outputDirBoDir);
                            self::actionBusinessObjectsGenerated($package, $fileName);
                        }

                    }

                    break;

                }

            } while(true);

        }

    }

    private function actionBusinessObjectsGenerated($package, $fileName, $customDir = null) {

        $params = ["package" => $package, "model" => $fileName];
        if(!is_null($customDir)) {
            $params["dir"] = $customDir;

        }

        $this->info("Please input your Create Basic By ID/INDEX");
        $this->line("1. CREATE BASIC BF BY ID");
        $this->line("2. CREATE BASIC BF BY INDEX");
        $this->info("If you only generate BY ID input 1, if only generate BY INDEX 2, if Generate BY ID and BY INDEX input 1,2");

        $argsBasicBf = $this->ask("Please input number: ");

        if($argsBasicBf == "1") {
            Artisan::call("leaf:create-basic-bf-id", $params);
            $this->info("Business Objects already generated");
        } else if($argsBasicBf == "2") {
            $indexingColumn = $this->ask("Please input index");
            $params["index"] = $indexingColumn;
            Artisan::call("leaf:create-basic-bf-index", $params);
            $this->info("Business Objects already generated");
        } else if($argsBasicBf == "1,2") {
            $indexingColumn = $this->ask("Please input index");
            Artisan::call("leaf:create-basic-bf-id", $params);

            $params["index"] = $indexingColumn;

            Artisan::call("leaf:create-basic-bf-index", $params);

            $this->info("Business Objects already generated");
        } else {

            $this->error("not valid");

        }

    }

    private function generateFile($path, $content){
        $f = fopen($path, "w");
        fwrite($f,"<?php\n\n");
        fwrite($f, $content);
        fclose($f);
    }

}
