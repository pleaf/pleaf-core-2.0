<?php
namespace Sts\PleafCore\Util;

use App;

class BusinessObject {

    public function call($serviceName, $inputDto) {

        $service = App::make($serviceName);

        return $response = $service->execute($inputDto);

    }

}