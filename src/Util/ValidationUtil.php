<?php 
namespace Sts\PleafCore\Util;

use Sts\PleafCore\CoreException;
use DateTime;

/**
 * 
 * @author Yang Yang
 *
 */
class ValidationUtil {
	
	public static function valBlankOrNull(array $dto, $key){
				
		if(!isset($dto[$key])) {
			throw new CoreException (VALUE_CANNOT_NULL,[$key]);
		}

        if(trim(strval($dto[$key])) == _EMPTY_VALUE) {
            throw new CoreException (VALUE_CANNOT_NULL,[$key]);
        }
	}

	public static function valDtoContainsKey(array $dto, $key){
		
		if(!array_key_exists($key, $dto)){
			throw new CoreException (PARAMETER_NOT_SPECIFIED,[$key]);
		}
	}

	public static function valDatetime(array $dto, $key){
		
		ValidationUtil::valDtoContainsKey($dto, $key);

		$d = DateTime::createFromFormat("YmdHis", $dto[$key]);
		
		if(($d && $d->format("YmdHis") == $dto[$key]) == false){
			throw new CoreException (DATETIME_FORMAT_INVALID,[$key]);
		}
	}

	public static function valDate(array $dto, $key){

		ValidationUtil::valDtoContainsKey($dto, $key);
		
		$d = DateTime::createFromFormat("Ymd", $dto[$key]);
		
		if(($d && $d->format("Ymd") == $dto[$key]) == false){
			throw new CoreException (DATE_FORMAT_INVALID,[$key]);
		}
	}

	public static function valLong(array $dto, $key){

		ValidationUtil::valDtoContainsKey($dto, $key);
		
		if(!is_long($dto[$key])){
			throw new CoreException (VALUE_MUST_NUMERIC,[$key]);
		}
	}

	public static function valEmail(array $dto, $key){

		ValidationUtil::valDtoContainsKey($dto, $key);
		
		if(!filter_var($dto[$key], FILTER_VALIDATE_EMAIL)){
			throw new CoreException (EMAIL_FORMAT_INVALID,[$key]);
		}
	}

	public static function valTime(array $dto, $key){

		ValidationUtil::valDtoContainsKey($dto, $key);
		
		$d = DateTime::createFromFormat("His", $dto[$key]);
		
		if(($d && $d->format("His") == $dto[$key]) == false){
			throw new CoreException (TIME_FORMAT_INVALID,[$key]);
		}
	}

	public static function valPeriod(array $dto, $key){

		ValidationUtil::valDtoContainsKey($dto, $key);
		
		$d = DateTime::createFromFormat("Ym", $dto[$key]);
		
		if(($d && $d->format("Ym") == $dto[$key]) == false){
			throw new CoreException (PERIOD_FORMAT_INVALID,[$key]);
		}
	}

	public static function valNumber(array $dto, $key){

		ValidationUtil::valDtoContainsKey($dto, $key);
		
		if(!is_numeric($dto[$key])){
			throw new CoreException (VALUE_MUST_NUMBER,[$key]);
		}
	}

	public static function valNumeric(array $dto, $key){

		ValidationUtil::valDtoContainsKey($dto, $key);
		
		if(!is_int($dto[$key])){
			throw new CoreException (VALUE_MUST_NUMERIC,[$key]);
		}
	}

	public static function valHttpURL(array $dto, $key){

		ValidationUtil::valDtoContainsKey($dto, $key);
		
		if(!filter_var($dto[$key], FILTER_VALIDATE_URL)){
			throw new CoreException (HTTP_URL_FORMAT_INVALID,[$key]);
		}
	}

	public static function valValidIpAddress(array $dto, $key){

		ValidationUtil::valDtoContainsKey($dto, $key);
		
		if(!filter_var($dto[$key], FILTER_VALIDATE_IP)){
			throw new CoreException (INVALID_IP_ADDRESS_VALUE,[$key]);
		}
	}

}
