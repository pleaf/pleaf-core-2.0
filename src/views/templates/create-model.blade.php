namespace {{ $namespace }};

use Sts\PleafCore\BaseEntity;
use Sts\PleafCore\ModelTableName;

class {{ $name }} extends BaseEntity {

   use ModelTableName;

   protected $table      = "{{$table}}";
   protected $primaryKey = "{{$primary}}";
   public $caseAttribute = "{{$attribute}}";
   public $timestamps    = false;


}
