namespace {{ $namespace }};

use Sts\PleafCore\BusinessFunction;
use Sts\PleafCore\DefaultBusinessFunction;
use {{ $namespaceModel }};
use Log;

/**
 * @author {{ $author }}
 * @in
 *
 * @out
 */
class {{ $className }} extends DefaultBusinessFunction implements BusinessFunction {

    public function getDescription(){
        return "{{$className}}";
    }

    public function process($dto){
        {!! $paramKeyValue !!}

        ${{ $var }} = {{ $model }}::{!! $condition !!};

        $outputDto["exists"] = false;
        if(!is_null(${{ $var }})) {
            $outputDto["exists"] = true;
            $outputDto["{{$var}}Dto"]= ${{ $var }};
        }

        return $outputDto;
    }

}