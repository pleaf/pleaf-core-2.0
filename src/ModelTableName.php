<?php
/**
 * Created by Widana.
 * User: sts
 * Date: 10/04/18
 * Time: 14:25
 */

namespace Sts\PleafCore;


trait ModelTableName
{

    public static function getTableName()
    {
        return ((new self)->getTable());
    }

    public static function getPrimaryKey() {
        return ((new self)->getKeyName());
    }

}