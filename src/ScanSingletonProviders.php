<?php
namespace Sts\PleafCore;

trait ScanSingletonProviders {

    public function scan($dir = null) {
        $baseDir = $this->baseDir;
        if(is_null($dir)) {
            $this->globScan($baseDir);
        }
        $this->globScan($baseDir, $dir);
    }

    public function globScan($baseDir, $dir = null) {
        $pathToFile = !is_null($dir)? $baseDir."/".$dir : $baseDir;
        $listFile = glob("$pathToFile/*.php");
        foreach ($listFile as $file) {
            $baseName = basename($file, ".php");
            $this->registerService(lcfirst($baseName), $baseName);
        }
    }

    public function registerService($serviceName, $className) {
        $namespace = $this->namespace;
        $class = "$namespace"."$className";
        $this->app->singleton($serviceName, function() use ($class) {
            return new $class();
        });

    }

}