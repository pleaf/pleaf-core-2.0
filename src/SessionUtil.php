<?php
/**
 * Created by PhpStorm.
 * User: sts
 * Date: 3/22/16
 * Time: 4:32 PM
 */

namespace Sts\PleafCore;
use Session;

class SessionUtil
{
    public static function getTenantId(){
        if(Session::has(_PLEAF_SESS_USERS)){
            return Session::get(_PLEAF_SESS_USERS)["tenantId"];
        }
        return -1;
    }

    public static function getTenantKey(){
        if(Session::has(_PLEAF_SESS_USERS)){
            return Session::get(_PLEAF_SESS_USERS)["tenantKey"];
        }
        return -1;
    }

    public static function getUsername(){
        if(Session::has(_PLEAF_SESS_USERS)){
            return Session::get(_PLEAF_SESS_USERS)["username"];
        }
        return -99;
    }

    public static function getUserLoginId(){
        if(Session::has(_PLEAF_SESS_USERS)){
            return Session::get(_PLEAF_SESS_USERS)["userId"];
        }
        return -99;
    }

    public static function getRoleLoginId(){
        if(Session::has(_PLEAF_SESS_USERS)){
            return Session::get(_PLEAF_SESS_USERS)["roleDefaultId"];
        }
        return -99;
    }

    public static function getRoleDefaultName(){
        if(Session::has(_PLEAF_SESS_USERS)){
            return Session::get(_PLEAF_SESS_USERS)["roleDefaultName"];
        }
        return '';
    }

    public static function getOuDefaultId(){
        if(Session::has(_PLEAF_SESS_USERS)){
            return Session::get(_PLEAF_SESS_USERS)["ouDefaultId"];
        }
        return -99;
    }

    public static function getOuDefaultCode(){
        if(Session::has(_PLEAF_SESS_USERS)){
            return Session::get(_PLEAF_SESS_USERS)["ouDefaultCode"];
        }
        return '';
    }

    public static function getOuDefaultName(){
        if(Session::has(_PLEAF_SESS_USERS)){
            return Session::get(_PLEAF_SESS_USERS)["ouDefaultName"];
        }
        return '';
    }

    public static function getFullname(){
        if(Session::has(_PLEAF_SESS_USERS)){
            return Session::get(_PLEAF_SESS_USERS)["fullname"];
        }
        return '';
    }
        
}
