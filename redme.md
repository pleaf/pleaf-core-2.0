## About Leaf Common
1. Leaf Common support generated
    - Create Model
    - Create Basic BF by ID
    - Create Basic BF by INDEX
    - Showing List Packages

## Command
    1. php artisan leaf:package-list
    2. php artisan leaf:create-model {table} {package} {attribute (CAMEL)}
        - Support Attribute: UPPER,LOWER,CAMEL,SNAKE,STUDLY,UCFIRST
    3.php artisan leaf:create-basic-bf-id {package} {model} {dir}
    4.php artisan leaf:create-basic-bf-id {package} {model} {index} {dir}
        
    
    